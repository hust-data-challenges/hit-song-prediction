from source.solver.solver import Solver
from source.utils.config_parser import cfg_from_file
from source.moduler_factory.transform_factory import get_transformer
import click


@click.command()
@click.option("--yml_file")
@click.option("--n_epochs")
@click.option("--info")
def run(yml_file, info, n_epochs):
    config = cfg_from_file(yml_file)
    transformer = get_transformer(config.transfomer)
    solver = Solver(config, transformer)
    solver.train(config, n_epochs)
    # df = pd.read_csv(info, sep='\t', encoding='utf-8')
    # dataset = Freesound_logmel(config, df, 'train', transformer)
    # print(len(dataset), '*' * 100)


if __name__ == '__main__':
    run()
