from source.utils.data_processing import get_info_dataFrame, feature_generate, get_infosong
from source.utils.common import make_dirs
import pandas as pd
import click
import os
import numpy as np

@click.command()
@click.option("--data_dir", required=False, default='storage/hitsong', type=str)
@click.option("--input_file", required=False, default='storage/dataset_info.tsv', type=str)
@click.option("--output_file", required=False, default='storage/final_data.tsv', type=str)
def data_prepare(data_dir, input_file, output_file):
    df = get_info_dataFrame(input_file, data_dir)
    # prepare_shards(df, processed_dir, num_shards)
    df = get_infosong(df)
    df.to_csv(output_file, sep='\t', encoding='utf-8')
    # print(np.isnan(df.iloc[1744]['danceability']))
    # with open('storage/miss_song.txt', 'w') as f:
    #     for i in range(len(df)):
    #         if np.isnan(df.iloc[i]['danceability']):
    #             f.write(df.iloc[i]['title'] + "\t" + df.iloc[i]['artist_name'] + "\t" + df.iloc[i]['composers_name'] + "\t" + str(df.iloc[i]['label']) + '\n')

@click.command()
@click.option("--data_dir", required=False, default='storage/hitsong', type=str)
@click.option("--processed_dir", required=False, default='storage/processed_data')
@click.option("--feature", required=True)
@click.option("--info_file", required=True, default='storage/dataset_info.tsv')
def run(data_dir, processed_dir, info_file, feature):
    make_dirs(processed_dir, "/".join(data_dir.split('/')[:-1]))
    df = pd.read_csv(info_file, sep='\t', encoding='utf-8')
    feature_generate(df, feature)

if __name__ == '__main__':
    # data_prepare()
    run()