from source.utils.data_processing import *

if __name__ == '__main__':
    fname = "storage/samples/audio_0.mp3"
    raw_sounds = load_sound_files([fname])
    plot_waves("", raw_sounds)
    # plot_specgram("", raw_sounds)
    # plot_log_power_specgram("", raw_sounds)