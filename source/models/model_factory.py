def get_model(name, *args, **kwargs):
    p = globals().copy()
    p.update(globals())
    method = p.get(name)
    if not method:
        raise NotImplementedError('Method %s not implement' % name)
    return method