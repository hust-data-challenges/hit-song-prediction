import pandas as pd
import os
import librosa
import librosa.display
# import librosa.logamplitude
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import specgram
from source.utils.common import save_data, time_diff
from multiprocessing import Pool
from source.utils.config import config
from sklearn.utils import shuffle
import torch
import math
from tqdm import tqdm
from string import digits


import spotipy
from spotipy.oauth2 import SpotifyClientCredentials
import sys

# dataset_dir = ""
# processed_dir = ""
# test_flag = False

def wavelist_prepare(dataset_dir):
    train_dir = f'{dataset_dir}/train'
    test_dir = f'{dataset_dir}/test'
    waves_train = sorted(os.listdir(train_dir))
    waves_test = sorted(os.listdir(test_dir))
    print(len(waves_train) + len(waves_test))
    df_train = pd.DataFrame({'fname': waves_train})
    df_train['train0/test1'] = pd.DataFrame(0 for i in range(len(waves_train)))

    df_test = pd.DataFrame({'fname': waves_test})
    df_test['train0/test1'] = pd.DataFrame(1 for i in range(len(waves_test)))

    df = df_train.append(df_test)
    df.set_index('fname', inplace=True)
    df.to_csv('./wavelist.csv')

def data_preprocessing(df, data_dir):
    df['time_diff'] = df['release_time'].apply(lambda t: time_diff(t))
    # df['fname'] = df['ID'].apply(lambda id: f"{id}.mp3")
    # train0_test1 = [0]*len(df)
    # indices, labels = list(range(len(df))), df['label']
    # train_indices, test_indices, _, __ = train_test_split(indices, labels, test_size=test_size)
    # for i in test_indices:
    #     train0_test1[i] = 1
    # df['train0/test1']= train0_test1
    df.set_index('ID', inplace=True)
    df = shuffle(df)
    return df


def get_info_dataFrame(output_file, data_dir=None):
    if os.path.isfile(output_file):
        df = pd.read_csv(output_file, delimiter='\t', encoding='utf-8', index_col=0)
    else:
        train_set = data_dir + "/train_info.tsv"
        train_rank = data_dir + "/train_rank.csv"
        set_df = pd.read_csv(train_set, delimiter='\t', encoding='utf-8')
        rank_df = pd.read_csv(train_rank)
        df = pd.merge(set_df, rank_df, on=['ID'])
        df = data_preprocessing(df, data_dir)
        df.to_csv(output_file, sep='\t')
    return df


def load_sound_files(file_paths):
    raw_sounds = []
    for fp in file_paths:
        X, sr = librosa.load(fp)
        raw_sounds.append(X)
    return raw_sounds


def plot_waves(sound_names, raw_sounds):
    i = 1
    fig = plt.figure(figsize=(25, 60), dpi=900)
    for n, f in zip(sound_names, raw_sounds):
        plt.subplot(10, 1, i)
        librosa.display.waveplot(np.array(f), sr=22050)
        plt.title(n.title())
        i += 1
    plt.suptitle("Figure 1: Waveplot", x=0.5, y=0.915, fontsize=18)
    plt.show()


def plot_specgram(sound_names, raw_sounds):
    i = 1
    fig = plt.figure(figsize=(25, 60), dpi=900)
    for n, f in zip(sound_names, raw_sounds):
        plt.subplot(10, 1, i)
        specgram(np.array(f), Fs=22050)
        plt.title(n.title())
        i += 1
    plt.suptitle("Figure 2: Spectrogram", x=0.5, y=0.915, fontsize=18)
    plt.show()


def plot_log_power_specgram(sound_names, raw_sounds):
    i = 1
    fig = plt.figure(figsize=(25, 60), dpi=900)
    for n, f in zip(sound_names, raw_sounds):
        plt.subplot(10, 1, i)
        D = librosa.logamplitude(np.abs(librosa.stft(f)) ** 2, ref_power=np.max)
        librosa.display.specshow(D, x_axis='time', y_axis='log')
        plt.title(n.title())
        i += 1
    plt.suptitle("Figure 3: Log power spectrogram", x=0.5, y=0.915, fontsize=18)
    plt.show()


def wav_to_pickle(df):
    pool = Pool(10)
    pool.map(tsfm_wave, df.iterrows())


def wav_to_logmel(df):
    pool = Pool(10)
    pool.map(tsfm_logmel, df.iterrows())


def wav_to_mfcc(df):
    pool = Pool(10)
    pool.map(tsfm_mfcc, df.iterrows())


def tsfm_wave(row):
    dataset_dir = 'storage/hitsong/'
    processed_dir = 'storage/processed_data/'
    test_flag = False
    # global data_dir, processed_dir, test_flag
    sr = 44100
    item = row[1]

    if not test_flag:
        file_path = os.path.join(dataset_dir + "/train", str(item['ID']) + '.mp3')
    else:
        file_path = os.path.join(dataset_dir + "/test", str(item['ID']) + '.mp3')

    data, _ = librosa.load(file_path, sr=sr, res_type='kaiser_best')
    p_name = os.path.join(f'{processed_dir}/data-44100', str(item['ID']) + '.pkl')
    # p_name = os.path.join(f'{processed_dir}/npz_data', os.path.splitext(item['fname'])[0] + '.npz')
    save_data(p_name, data)


def tsfm_logmel(row):
    dataset_dir = 'storage/hitsong/'
    processed_dir = 'storage/processed_data/'
    test_flag = False
    item = row[1]
    p_name = os.path.join(f'{processed_dir}/logmel+delta_w80_s10_m64', str(item['ID']) + '.pkl')
    if not os.path.exists(p_name):
        if not test_flag:
            file_path = os.path.join(dataset_dir + "/train", str(item['ID']) + '.mp3')
        else:
            file_path = os.path.join(dataset_dir + "/test", str(item['ID']) + '.mp3')

        data, sr = librosa.load(file_path, config.sampling_rate)

        # some audio file is empty, fill logmel with 0.
        if len(data) == 0:
            print("empty file:", file_path)
            logmel = np.zeros((config.n_mels, 150))
            feats = np.stack((logmel, logmel, logmel))
        else:
            melspec = librosa.feature.melspectrogram(data, sr,
                                                     n_fft=config.n_fft, hop_length=config.hop_length,
                                                     n_mels=config.n_mels)

            logmel = librosa.core.power_to_db(melspec)

            delta = librosa.feature.delta(logmel)
            accelerate = librosa.feature.delta(logmel, order=2)

            feats = np.stack((logmel, delta, accelerate))  # (3, 64, xx)

        save_data(p_name, feats)


def tsfm_mfcc(row):
    dataset_dir = 'storage/hitsong/'
    processed_dir = 'storage/processed_data/'
    test_flag = False
    item = row[1]
    p_name = os.path.join(f'{processed_dir}/mfcc+delta_w80_s10_m64', str(item['ID'])  + '.pkl')
    if not os.path.exists(p_name):
        # print(p_name)
        if not test_flag:
            file_path = os.path.join(dataset_dir + "/train", str(item['ID']) + '.mp3')
        else:
            file_path = os.path.join(dataset_dir + "/test", str(item['ID']) + '.mp3')

        data, sr = librosa.load(file_path, config.sampling_rate)

        # some audio file is empty, fill logmel with 0.
        if len(data) == 0:
            print("empty file:", file_path)
            mfcc = np.zeros((config.n_mels, 150))
            feats = np.stack((mfcc, mfcc, mfcc))
        else:
            mfcc = librosa.feature.mfcc(data, sr,
                                        n_fft=config.n_fft,
                                        hop_length=config.hop_length,
                                        n_mfcc=config.n_mels)
            delta = librosa.feature.delta(mfcc)
            accelerate = librosa.feature.delta(mfcc, order=2)

            feats = np.stack((mfcc, delta, accelerate))  # (3, 64, xx)

        save_data(p_name, feats)

def mixup(data, one_hot_labels, alpha=1):
    batch_size = data.size()[0]

    weights = np.random.beta(alpha, alpha, batch_size)

    weights = torch.from_numpy(weights).type(torch.FloatTensor)

    #  print('Mixup weights', weights)
    index = np.random.permutation(batch_size)
    x1, x2 = data, data[index]

    x = torch.zeros_like(x1)
    for i in range(batch_size):
        for c in range(x.size()[1]):
            x[i][c] = x1[i][c] * weights[i] + x2[i][c] * (1 - weights[i])

    y1 = one_hot_labels
    y2 = one_hot_labels[index]

    y = torch.zeros_like(y1)

    for i in range(batch_size):
        y[i] = y1[i] * weights[i] + y2[i] * (1 - weights[i])

    return x, y

def prepare_shards(df, processed_dir, num_shards):
    if not os.path.exists(processed_dir + "/shards"):
        os.mkdir(processed_dir + "/shards")
    for rank in range(1, 11):
        df_temp = df[df['label'] == rank]
        assert len(df_temp) > 0
        shard_size = math.ceil(len(df_temp) / num_shards)
        indices = df_temp.index.values
        # indices = list(range(len(df_temp)))
        np.random.shuffle(indices)
        for s in range(num_shards):
            start = s * shard_size
            end = min(start + shard_size, len(df_temp))
            shard_df = df_temp[df_temp.index.isin(indices[start:end])]
            assert max(df_temp.index) == max(indices) and min(df_temp.index) == min(indices)
            assert len(shard_df) > 0
            shard_file_name = processed_dir + "/shards/shard-{s}.csv"
            write_csv_header = not os.path.isfile(shard_file_name)
            with open(shard_file_name, "a") as shard_file:
                shard_df.to_csv(shard_file, header=write_csv_header)

def feature_generate(df, feature):
    # global data_dir, processed_dir, test_flag
    # data_dir = data_dir_
    # processed_dir = processed_dir_
    # test_flag = test_flag_
    if feature == 'wave':
        wav_to_pickle(df)
    elif feature == 'logmel':
        wav_to_logmel(df)
    elif feature == 'mfcc':
        wav_to_mfcc(df)
    else:
        raise ValueError(f"This {feature} is not supporting !!!")

def get_spotify(title, artist, f):
    # Make your own Spotify app at https://beta.developer.spotify.com/dashboard/applications
    title = str_processing(title)
    artist = str_processing(artist)
    client_id = 'd525e8fc80c34a15a8cc86377fb611b5'
    client_secret = 'a68d4567ecda4b39963c2855f9b316ea'
    uri = ''
    search_items = [title + ' ' + artist, title, artist]
    idx = 0
    client_credentials_manager = SpotifyClientCredentials(client_id=client_id, client_secret=client_secret)
    sp = spotipy.Spotify(client_credentials_manager=client_credentials_manager)
    sp.trace = False
    features = None
    while features == None:
        if idx >= len(search_items):
            break
        search_querry = search_items[idx]
        idx += 1
        result = sp.search(search_querry)
        for i in result['tracks']['items']:
            # Find a songh that matches title and artist
            if (i['artists'][0]['name'] == artist) and (i['name'] == title):
                uri = i['uri']
                break
        else:
            try:
                # Just take the first song returned by the search (might be named differently)
                uri = result['tracks']['items'][0]['uri']
            except:
                # No results for artist and title
                print("Cannot Find URI")

        client_credentials_manager = SpotifyClientCredentials(client_id=client_id, client_secret=client_secret)
        sp = spotipy.Spotify(client_credentials_manager=client_credentials_manager)
        sp.trace = False
        features = sp.audio_features(uri)[0]

    if features != None:
        del features['id']
        del features['uri']
        del features['track_href']
        del features['analysis_url']
        del features['type']
    else:
        f.writelines(title + " " + artist + "\n")
    return features

def str_processing(s):
    remove_digits = str.maketrans('', '', digits)
    s = s.translate(remove_digits)
    s = s.replace("(", "").replace(")", "")
    s = s.lower()
    s = s.replace("beat", "")
    return s

def get_infosong(df):
    f = open("Miss_Song.txt", "w")
    n = len(df)
    keys = ['danceability', 'energy', 'key', 'loudness', 'mode', 'speechiness', 'acousticness',
            'instrumentalness', 'liveness', 'valence', 'tempo', 'duration_ms', 'time_signature']
    key_map = {}
    for key in keys:
        key_map[key] = []
    for i in range(2000):
        item = df.iloc[i]
        try:
            if np.isnan(item['danceability']):
                print(item['title'], item['artist_name'], item['label'], "*" * 100)
                features = get_spotify(item['title'], item['artist_name'], f)
                # features = None
                for key in key_map:
                    if features:
                        df.set_value(i, key, features[key])
        except:
            print("Miss when check information of song: " + item['title'] + '\n')
            pass
    # print(df.iloc[4030])

    f.close()
    return df