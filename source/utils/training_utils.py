import torch
import torch.functional as F
import shutil
import  time
import logging

class AverageMeter(object):
    """Computes and stores the average and current value"""
    def  __init__(self):
        self.reset()

    def  reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

def cross_entropy_onehot(input, target, size_average=True):
    """
    Cross entropy  that accepts soft targets (like [0, 0.1, 0.1, 0.8, 0]).
    """
    assert input.size() == target.size()

    if size_average:
        return torch.mean(torch.sum(-target * F.log_softmax(input, dim=1), dim=1))
    else:
        return torch.sum(torch.sum(-target * F.log_softmax(input, dim=1), dim=1))

def save_checkpoint(state, is_best, fold, config, filename='../model/checkpoint.pth.tar'):
    torch.save(state, filename)
    if is_best:
        best_name = config.model_dir + '/model_best.' + str(fold) + '.pth.tar'
        shutil.copyfile(filename, best_name)

def accuracy(output, target, topk=(1,)):
    """Computes the precision@k for the specified values of k"""
    with torch.no_grad():
        maxk = max(topk)
        batch_size = target.size(0)

        _, pred = output.topk(maxk, 1, True, True)
        pred = pred.t()
        correct = pred.eq(target.view(1, -1).expand_as(pred))

        res = []
        for k in topk:
            correct_k = correct[:k].view(-1).float().sum(0, keepdim=True)
            res.append(correct_k.mul_(100.0 / batch_size))
        return res

def accurate_on_val(model, criterion, val_loader, config, fold):
    batch_time = AverageMeter()
    losses = AverageMeter()
    top1 = AverageMeter()
    top3 = AverageMeter()

    # switch to evaluate mode
    model.eval()

    with torch.no_grad():
        end = time.time()
        for i, (input, target) in enumerate(val_loader):

            if config.cuda:
                input, target = input.cuda(), target.cuda(non_blocking=True)

            # compute output
            output = model(input)
            loss = criterion(output, target)
            # measure accuracy and record loss
            prec1, prec3 = accuracy(output, target, topk=(1, 3))
            losses.update(loss.item(), input.size(0))
            top1.update(prec1[0], input.size(0))
            top3.update(prec3[0], input.size(0))

            # measure elapsed time
            batch_time.update(time.time() - end)
            end = time.time()

            if i % config.print_freq == 0:
                logging.info('Test. '
                      'Time {batch_time.val:.1f} '
                      'Loss {loss.avg:.2f} '
                      'Prec@1 {top1.val:.2f}({top1.avg:.2f}) '
                      'Prec@3 {top3.val:.2f}({top3.avg:.2f})'.format(
                    batch_time=batch_time, loss=losses,
                    top1=top1, top3=top3))

        logging.info(' * Prec@1 {top1.avg:.3f} Prec@3 {top3.avg:.3f}'
              .format(top1=top1, top3=top3))

    return top1.avg, top3.avg
