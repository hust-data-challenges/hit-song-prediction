import os
import matplotlib as mpl
import matplotlib.pyplot as plt
import pickle
import datetime as dt
from sklearn.model_selection import StratifiedKFold
import numpy as np

mpl.rc('axes', labelsize=14)
mpl.rc('xtick', labelsize=12)
mpl.rc('ytick', labelsize=12)

def save_fig(image_path, tight_layout=True, resolution=300):
    print("Saving figure", image_path)
    if tight_layout:
        plt.tight_layout()
    plt.savefig(image_path, format=image_path.split(".")[-1], dpi=resolution)

def save_data(filename, data):
    pickle.dump(data, open(filename, 'wb'), protocol=pickle.HIGHEST_PROTOCOL)
    print("writing file '{}'".format(filename), flush=True)
    # np.savez_compressed(filename, data=data)

def load_data(filename):
    return pickle.load(open(filename, "rb"), encoding='latin1')

def make_dirs(processed_data, storage_dir):
    processed_data = os.path.abspath(".") + "/" + processed_data
    if not os.path.exists(processed_data):
        os.mkdir(processed_data)

    dirs = [f'{processed_data}/data-44100', f'{processed_data}/npz_data', f'{storage_dir}/prediction', f'{storage_dir}/log',
            f'{storage_dir}/models', f'{storage_dir}/submission', f'{processed_data}/logmel+delta_w80_s10_m64',
            f'{processed_data}/mfcc+delta_w80_s10_m64']
    assert os.path.exists(processed_data) == True
    for dir in dirs:
        if not os.path.exists(dir):
            os.mkdir(dir)
            print(f'Make dir {dir} successfully !!!')

def time_diff(release_time):
    start = dt.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    end = release_time
    start_dt = dt.datetime.strptime(start, '%Y-%m-%d %H:%M:%S')
    end_dt = dt.datetime.strptime(end, '%Y-%m-%d %H:%M:%S')
    diff = (end_dt - start_dt)
    return diff.seconds/60

def kfold_split(n_splits, values, classes):
    skf = StratifiedKFold(n_splits=n_splits, shuffle=True, random_state=42)
    for train_value_indexes, test_value_indexes in skf.split(values, classes):
        train_values = [values[i] for i in train_value_indexes]
        test_values = [values[i] for i in test_value_indexes]
        yield train_values, test_values

def get_fold_dataset(df, fold):
    train_indices, val_indices = list(kfold_split(3,
                    range(len(df)), df))[fold]
    return df[train_indices], df[val_indices]
