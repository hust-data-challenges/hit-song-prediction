import logging
import time

from torch.backends import cudnn
from torch.utils.data import DataLoader

from source.datasets.dataset_factory import get_dataset
from source.models.model_factory import get_model
from source.moduler_factory.criterion_factory import get_criterion
from source.moduler_factory.lr_scheduler_factory import get_scheduler
from source.moduler_factory.optimizer_factory import get_optimizer
from source.utils.common import get_fold_dataset
from source.utils.training_utils import AverageMeter, save_checkpoint, accurate_on_val


class Solver:
    def __init__(self, config, transform):
        self.df = []
        self.dataset_fn = get_dataset(config.dataset, transform)
        self.batch_size = config.solver.batch_size
        self.num_folds = config.solver.num_folds
        self.n_jobs = config.solver.num_workers
        self.config = config
        self.cuda = config.cuda == 'True'
        if self.cuda:
            cudnn.benchmark = True
        # self.init(config)

    def init(self, config):
        self.model = get_model(config.model)
        if self.cuda:
            self.model = self.model.cuda()

        self.train_criterion = get_criterion(config.mixup)
        self.val_criterion = get_criterion("cross_entropy")
        self.optimizer = get_optimizer(self.model, config.solver.optimizer)
        self.lr_scheduler = get_scheduler(self.optimizer, config.scheduler)

    def train(self, config, n_epochs):
        for fold in range(len(self.num_folds)):
            self.init(config)
            self.train_on_fold(config, self.model, self.train_criterion, self.optimizer, fold, n_epochs)
        pass

    def train_on_fold(self, config, model, train_criterion, val_criterion, optimizer, fold, n_epochs):
        start = time.time()
        train_df, val_df = get_fold_dataset(self.df, fold)
        logging.info("Fold {0}, Train samples:{1}, val samples:{2}"
                     .format(fold, len(train_df), len(val_df)))
        train_set = self.dataset_fn(train_df, config.mixup)
        val_set = self.dataset_fn(val_df, config.mixup)

        train_loader = DataLoader(train_set, batch_size=self.batch_size, shuffle=True, num_workers=self.n_jobs)
        val_loader = DataLoader(val_set, batch_size=self.batch_size, shuffle=False, num_workers=self.n_jobs)

        # TODO: training model in here
        self.model.train()
        best_prec1 = 0
        for epoch in range(n_epochs):
            self.lr_scheduler.step()
            self.train_on_epoch(config, train_loader, model, train_criterion, optimizer, fold, epoch)
            prec1, prec3 = accurate_on_val(model, val_criterion, val_loader, config, fold)
            if not config.debug:
                is_best = prec1 > best_prec1
                best_prec1 = max(prec1, best_prec1)
                save_checkpoint({
                    'epoch': epoch + 1,
                    'arch': config.arch,
                    # 'model': model,
                    'state_dict': model.state_dict(),
                    'best_prec1': best_prec1,
                    'optimizer': optimizer.state_dict(),
                }, is_best, fold, config,
                    filename=config.model_dir + '/checkpoint.pth.tar')

        logging.info(' *** Best Prec@1 {prec1:.3f}'
                     .format(prec1=best_prec1))
        time_on_fold = time.strftime('%Hh:%Mm:%Ss', time.gmtime(time.time() - start))
        logging.info("--------------Time on fold {}: {}--------------\n"
                     .format(fold, time_on_fold))
        pass

    def train_on_epoch(self, config, train_loader, model, criterion, optimizer, fold, epoch):
        batch_time = AverageMeter()
        data_time = AverageMeter()
        losses = AverageMeter()
        top1 = AverageMeter()
        top3 = AverageMeter()
        model.train()

        end = time.time()

        for i, (input, target) in enumerate(train_loader):

            # if self.config.mixup:
            #     one_hot_labels = make_one_hot(target)
            #     input, target = mixup(input, one_hot_labels, alpha=3)

            # measure data loading time
            data_time.update(time.time() - end)

            if self.cuda:
                input, target = input.cuda(), target.cuda(non_blocking=True)

            # Compute output
            #  print("input:", input.size(), input.type())  # ([batch_size, 1, 64, 150])
            output = model(input)
            #  print("output:", output.size(), output.type())  # ([bs, 41])
            #  print("target:", target.size(), target.type())  # ([bs, 41])
            loss = criterion(output, target)

            # measure accuracy and record loss
            # prec1, prec3 = accuracy(output, target, topk=(1, 3))
            losses.update(loss.item(), input.size(0))
            # top1.update(prec1[0], input.size(0))
            # top3.update(prec3[0], input.size(0))

            # Compute gradient and do SGD step
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            # Measure elapsed time
            batch_time.update(time.time() - end)
            end = time.time()

            if i % config.print_freq == 0:
                logging.info('F{fold} E{epoch} lr:{lr:.4g} '
                             'Time {batch_time.val:.1f}({batch_time.avg:.1f}) '
                             'Data {data_time.val:.1f}({data_time.avg:.1f}) '
                             'Loss {loss.avg:.2f}'.format(
                    i, len(train_loader), fold=fold, epoch=epoch,
                    lr=optimizer.param_groups[0]['lr'], batch_time=batch_time,
                    data_time=data_time, loss=losses))

        return top1.avg, top3.avg
