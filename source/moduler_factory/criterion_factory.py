import torch
import torch.nn as nn
import torch.functional as F

def cross_entropy_one_hot(input, target, size_average=True):
    """
    Cross entropy  that accepts soft targets (like [0, 0.1, 0.1, 0.8, 0]).
    """
    assert input.size() == target.size()

    if size_average:
        return torch.mean(torch.sum(-target * F.log_softmax(input, dim=1), dim=1))
    else:
        return torch.sum(torch.sum(-target * F.log_softmax(input, dim=1), dim=1))

def cross_entropy():
    return nn.CrossEntropyLoss()

def get_criterion(loss_type):
    if loss_type == 'mixup':
        criterion = cross_entropy_one_hot
    elif loss_type == "cross_entropy":
        criterion = cross_entropy
    # elif loss_type == "scce":
    #     criterion = SoftCrossEntropyLoss()
    # elif loss_type == "sbs":
    #     criterion = SoftBootstrapingLoss(beta=bootstraping_loss_ratio)
    # elif loss_type == "hbs":
    #     criterion = HardBootstrapingLoss(beta=bootstraping_loss_ratio)
    # elif loss_type == "focal":
    #     criterion = FocalLoss()
    # elif loss_type == "topk_svm":
    #     criterion = SmoothSVM(n_classes=num_classes, k=3, tau=1., alpha=1.)
    # elif loss_type == "center":
    #     criterion = CceCenterLoss(num_classes=num_classes, alpha=0.5)
    else:
        raise Exception("Unsupported loss type: '{}".format(loss_type))
    return criterion