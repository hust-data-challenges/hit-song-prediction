from source.datasets.logmel_loader import Freesound_logmel

def get_dataset(config, mode='train', split=True, transform=None):
    f = globals().get(config.name)

    return f(config.dir,
             split=split,
             transform=transform,
             **config.params)