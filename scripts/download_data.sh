wget https://dl.challenge.zalo.ai/hitsong/train_info.tsv -p storage/datasets
wget https://dl.challenge.zalo.ai/hitsong/train_rank.csv -p storage/datasets
wget https://dl.challenge.zalo.ai/hitsong/test_info.tsv -p storage/datasets
wget https://dl.challenge.zalo.ai/hitsong/sample_submission.csv -p storage/datasets
wget https://dl.challenge.zalo.ai/hitsong/train.zip -p storage/datasets
wget https://dl.challenge.zalo.ai/hitsong/test.zip -p storage/datasets

mv dl.challenge.zalo.ai/* storage/hitsong
rm -r dl.challenge.zalo.ai

unzip storage/hitsong/train.zip -d storage/hitsong
unzip storage/hitsong/test.zip -d storage/hitsong
